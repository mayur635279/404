﻿namespace pingpong.v1
{
     partial class PingPongForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gamePanel = new System.Windows.Forms.Panel();
            this.ball = new System.Windows.Forms.PictureBox();
            this.start = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.gamePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.ball)).BeginInit();
            this.SuspendLayout();
            // 
            // gamePanel
            // 
            this.gamePanel.BackColor = System.Drawing.Color.SeaGreen;
            this.gamePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gamePanel.Controls.Add(this.ball);
            this.gamePanel.Location = new System.Drawing.Point(10, 12);
            this.gamePanel.Margin = new System.Windows.Forms.Padding(2);
            this.gamePanel.Name = "gamePanel";
            this.gamePanel.Size = new System.Drawing.Size(599, 300);
            this.gamePanel.TabIndex = 0;
            // 
            // ball
            // 
            this.ball.BackColor = System.Drawing.Color.DodgerBlue;
            this.ball.Location = new System.Drawing.Point(100, 75);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(24, 25);
            this.ball.TabIndex = 0;
            this.ball.TabStop = false;
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(10, 315);
            this.start.Margin = new System.Windows.Forms.Padding(2);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(100, 30);
            this.start.TabIndex = 1;
            this.start.Text = "Spiel starten";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.StartGame);
            // 
            // timer
            // 
            this.timer.Interval = 120;
            this.timer.Tick += new System.EventHandler(this.EventHandler);
            // 
            // PingPongForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 355);
            this.Controls.Add(this.start);
            this.Controls.Add(this.gamePanel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PingPongForm";
            this.Text = "Ping-Pong Spiel";
            this.gamePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.ball)).EndInit();
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.PictureBox ball;
        private System.Windows.Forms.Panel gamePanel;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Timer timer;

        #endregion

        // private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer;
        // private Microsoft.VisualBasic.PowerPacks.OvalShape ball;
    }
}